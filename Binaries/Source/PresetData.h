/* (Auto-generated binary data file). */

#ifndef BINARY_PRESETDATA_H
#define BINARY_PRESETDATA_H

namespace PresetData
{
    extern const char*  externalFiles;
    const int           externalFilesSize = 58;

    extern const char*  images;
    const int           imagesSize = 21736;

    extern const char*  preset;
    const int           presetSize = 27098;

}

#endif
