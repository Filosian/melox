
#include "JuceHeader.h"
#include "PresetData.h"

REGISTER_STATIC_DSP_LIBRARIES()
{
	REGISTER_STATIC_DSP_FACTORY(HiseCoreDspFactory);
}
#if USE_COPY_PROTECTION
RSAKey Unlocker::getPublicKey() { return RSAKey(""); };
#endif
AudioProcessor* JUCE_CALLTYPE createPluginFilter() { CREATE_PLUGIN(nullptr, nullptr); }

AudioProcessor* StandaloneProcessor::createProcessor() { return nullptr; }
String ProjectHandler::Frontend::getProjectName() { return "Melox"; };
String ProjectHandler::Frontend::getCompanyName() { return "Sampleson"; };
String ProjectHandler::Frontend::getCompanyWebsiteName() { return "http://sampleson.com"; };
String ProjectHandler::Frontend::getVersionString() { return "0.1.0"; };

CREATE_FRONTEND_BAR(DefaultFrontendBar)

